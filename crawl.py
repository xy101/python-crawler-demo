# -*- coding: utf-8 -*-
#引入需要的包
import json
import time
import requests
print('记得一键三连哦！！！ Python爬虫开始')

headers = {
"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36",
}
#存放最终数据
result = []
# 填写up主的uid,此处是用我自己的uid（@小雨在烦恼什么)
uid = "163531251"

# 获取单个视频数据
def getVideoDetail(aid, data):
	url= f"http://api.bilibili.com/archive_stat/stat?aid={aid}&type=jsonp"
	html = requests.get(url, headers=headers)
	js = json.loads(html.text)
	# 将数据添加到result中

	keys = ['view', 'danmaku', 'reply', 'favorite', 'coin', 'share', 'like']
	for key in keys:
	    data[key] = js["data"][key]

# # 获取视频列表,请求b站开发的接口地址
url = f"https://api.bilibili.com/x/space/arc/search?mid={uid}&ps=&tid=0&pn=1&keyword=&order=pubdate&jsonp=jsonp"

html = requests.get(url, headers=headers)
js = json.loads(html.text)

# 将视频列表存放近进result
for item in js['data']['list']['vlist']:
    res = {"aid":item['aid'],"bvid":item['bvid'], "title":item['title'], "length":item['length'],}
    created = item["created"]
    ret = time.localtime(created)
    res['created'] = time.strftime("%Y-%m-%d %H:%M", ret)
    result.append(res)


# 遍历视频列表，按aid获取单个视频数据，将结果存入result
for item in result:
	aid = item['aid']
	getVideoDetail(aid, item)

# 输出结果
print(result)




